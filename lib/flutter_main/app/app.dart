import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_base_app/flutter_main/app/route.dart';
import 'package:flutter_base_app/flutter_main/common/res/style.dart';
import 'package:flutter_base_app/flutter_main/common/stats_widgets.dart';
import 'package:flutter_base_app/flutter_main/common/tools.dart';
import 'package:flutter_base_app/flutter_main/screens/cancel_request/cancel_request_screen.dart';
import 'package:flutter_base_app/flutter_main/screens/home/home_screen.dart';
import 'package:flutter_base_app/flutter_main/screens/login/login_screens.dart';
import 'package:flutter_base_app/flutter_main/screens/order_submission_feedback/order_submission_feedback_Screen.dart';
import 'package:flutter_base_app/flutter_main/screens/order_submit_screen/transaction_submit_screen.dart';
import 'package:flutter_base_app/flutter_main/screens/past_request/past_order_screen.dart';
import 'package:flutter_base_app/flutter_main/screens/profile_settings/profile_setting_screen.dart';
import 'package:flutter_base_app/flutter_main/screens/provider/UserProfileModel.dart';
import 'package:flutter_base_app/flutter_main/screens/request_list/model/request.dart';
import 'package:flutter_base_app/flutter_main/screens/request_list/provider/TransactionModel.dart';
import 'package:flutter_base_app/flutter_main/storage/pref_manager.dart';
import 'package:flutter_base_app/generated/l10n.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:provider/provider.dart';

import 'app_model.dart';

class App extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return AppState();
  }
}

class AppState extends State<App> {


  final _app = AppModel();
  final _userProfileModel = UserProfileModel();
  final _transactionModel = TransactionModel();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext appContext) {
    return FutureBuilder(
        future: PrefManager().setupSharedPreferences(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done &&
              snapshot.data == true) {
            hideLoading();
            return ChangeNotifierProvider<AppModel>.value(
              value: _app,
              child: Consumer<AppModel>(
                builder: (context, value, child) {
                  return MultiProvider(
                    providers: [
                      // Provider<AppModel>.value(value: widget._app),
                      ChangeNotifierProvider.value(value: _app),
                      ChangeNotifierProvider.value(value: _transactionModel),
                      ChangeNotifierProvider.value(value: _userProfileModel),
                    ],
                    child: MaterialApp(
                      navigatorKey: navigatorKey,

                      ///todo include MaterialApp to new <consumer> decedent of AppLanguageModel
                      debugShowCheckedModeBanner: false,
                      supportedLocales: S.delegate.supportedLocales,
                      locale: Provider
                          .of<AppModel>(context)
                          .local,
                      localizationsDelegates: const [
                        S.delegate,
                        GlobalMaterialLocalizations.delegate,
                        GlobalWidgetsLocalizations.delegate,
                        GlobalCupertinoLocalizations.delegate,
                        DefaultCupertinoLocalizations.delegate,
                      ],
                      builder: EasyLoading.init(),
                      theme: getTheme(),
                      home: getNextScreen(),
                      routes: Routes.getAll(),
                    ),
                  );
                },
              ),
            );
          } else {
            return Container(
              alignment: Alignment.center,
              color: Color(0xffffffff),
              child: CircularProgressIndicator(),
            );
          }
        });
  }

// getNextScreen() {
//
//   return AddAssetsScreen();
//   if (PrefManager().getUserToken() != null) {
//     return HomeScreen();
//   } else {
//     return LoginScreen();
//   }
}

getNextScreen() {

  if (PrefManager().getUserToken() != null) {
    return HomeScreen();
  } else {
    print("PrefManager().getUserToken ---->" +
        PrefManager().getUserToken().toString());
    return LoginScreen();
  }
}
