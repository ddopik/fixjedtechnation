import 'package:flutter/cupertino.dart';
import 'package:flutter_base_app/generated/l10n.dart';

import 'exception_indicator.dart';

/// Indicates that no items were found.
class EmptyListIndicator extends StatelessWidget {
  EmptyListIndicator();
  EmptyListIndicator.viewSized(this.viewSize);

  ViewSize viewSize = ViewSize.MID;

  @override
  Widget build(BuildContext context) => ExceptionIndicator(
        title: S.of(context).noResultFound,
        message: S.of(context).emptyListMessage,
        assetName: 'assets/images/empty-box.png',
        viewSize: viewSize,
      );
}

enum ViewSize { SMALL, MID }
