import 'dart:io';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_base_app/flutter_main/app/app_model.dart';
import 'package:flutter_base_app/flutter_main/common/colors.dart';
import 'package:flutter_base_app/flutter_main/common/res/dimen_const.dart';
import 'package:flutter_base_app/flutter_main/common/res/font_const.dart';
import 'package:flutter_base_app/flutter_main/common/stats_widgets.dart';
import 'package:flutter_base_app/flutter_main/common/widgets/app_bar_back_button.dart';
import 'package:flutter_base_app/flutter_main/common/widgets/custom_action_button.dart';
import 'package:flutter_base_app/flutter_main/common/widgets/custom_image_loader.dart';
import 'package:flutter_base_app/flutter_main/screens/profile_settings/change_profile_photo_dialog_view.dart';
import 'package:flutter_base_app/flutter_main/screens/provider/UserProfileModel.dart';
import 'package:flutter_base_app/generated/l10n.dart';
import 'package:provider/provider.dart';

import 'changeProfileNameDialogView.dart';
import 'edit_phone_dialog_view.dart';
import 'model/user_profile_response.dart';

class ProfileSettingScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _ProfileSettingScreenState();
}

class _ProfileSettingScreenState extends State<ProfileSettingScreen> {
  UserProfileModel _userProfileModel;
  UserProfileResponse _userProfileResponse;
  TextEditingController _userNameController = TextEditingController();
  TextEditingController _phoneNumberController = TextEditingController();
  File _profileImagePath;

  @override
  void initState() {
    _userProfileModel = Provider.of<UserProfileModel>(context, listen: false);
    getUserData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        elevation: 0.0,
        leading: AppBarBackButton(),
        leadingWidth: 200,
      ),
      body: Container(
          // padding: EdgeInsets.all(20),
          child: SingleChildScrollView(child: getView())),
    );
  }

  getView() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Row(
          children: [
            Container(
              padding: EdgeInsets.symmetric(horizontal: 20),
              width: MediaQuery.of(context).size.width * .3,
              height: MediaQuery.of(context).size.height * .1,
              child: AutoSizeText(
                S.current.ProfileSetting,
                style: Theme.of(context)
                    .textTheme
                    .headline5
                    .copyWith(fontSize: text_head_size_2),
              ),
            ),
          ],
        ),
        Center(
          child: getImageProfileView(),
        ),
        renderLoginForm(),
        SizedBox(
          height: MediaQuery.of(context).size.height * .095,
        ),
        customActionButton(
            btnText: Text(
              S.of(context).SaveChanges,
              style: Theme.of(context)
                  .textTheme
                  .headline4
                  .copyWith(color: Colors.white, fontSize: text_size_1),
            ),
            width: MediaQuery.of(context).size.width * .9,
            height: MediaQuery.of(context).size.height * .065,
            btnColor: Color(0xff61ba66),
            btnRadius: 8.0,
            onPressed: () {
              updateProfileData();
            }),
      ],
    );
  }

  getImageProfileView() {
    return Container(
      width: MediaQuery.of(context).size.width * .5,
      child: Stack(
        alignment: Alignment.center,
        children: [
          getImageView(),
          Positioned(
            bottom: 10,
            right: 10,
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(23.0),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 5,
                    blurRadius: 7,
                    offset: Offset(0, 3), // changes position of shadow
                  ),
                ],
              ),
              child: IconButton(
                icon: Icon(
                  Icons.edit,
                  color: boring_green,
                ),
                onPressed: () {
                  changeProfilePhotoDialogView(
                      context: context,
                      onImageSelected: (imagePath) {
                        setState(() {
                          _profileImagePath = File(imagePath.path);
                          Navigator.of(context).pop();
                        });
                      });
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  getImageView() {
    print(
        "_profileImagePath?.path ----> " + _profileImagePath?.path.toString());
    print("_userProfileResponse?.imageUrl  ---->  " +
        _userProfileResponse?.imageUrl.toString());
    return ClipOval(
      child: (_profileImagePath?.path == null &&
              _userProfileResponse?.imageUrl != null)
          ? CustomImageLoader.image(
              url: _userProfileResponse?.imageUrl ?? '',
              width: 180,
              height: 180,
              fit: BoxFit.cover)
          : (_profileImagePath?.path != null)
              ? Image.file(
                  _profileImagePath,
                  width: 180,
                  height: 180,
                  fit: BoxFit.cover,
                )
              : Image(
                  image: AssetImage("assets/images/img_profile.jpeg"),
                  width: 180,
                  height: 180,
                  fit: BoxFit.cover,
                ),
    );
  }

  renderLoginForm() {
    return Form(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          // Create an Account

          // Rectangle 85
          SizedBox(height: MediaQuery.of(context).size.height * .04),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 40),
            width: MediaQuery.of(context).size.width,
            child: AutoSizeText(
              S.of(context).EditYourPersonalInfo,
              style: Theme.of(context).textTheme.bodyText1,
            ),
          ),
          SizedBox(height: MediaQuery.of(context).size.height * .02),
          Container(
            width: MediaQuery.of(context).size.width * .70,
            height: 45,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(8)),
                boxShadow: [
                  BoxShadow(
                      color: const Color(0x29000000),
                      offset: Offset(0, 3),
                      blurRadius: 6,
                      spreadRadius: 0)
                ],
                color: const Color(0xFFE7F5E8)),
            child: TextFormField(
              controller: _userNameController,
              autocorrect: false,
              enableSuggestions: false,
              readOnly: true,
              autofocus: false,
              style: TextStyle(color: french_blue),
              decoration: new InputDecoration(
                hintText: S.of(context).userName,
              ),
              onTap: () {
                changeProfileNameDialogView(
                    context: context,
                    firstName: _userProfileResponse?.firstName??"",
                    lastName: _userProfileResponse?.lastName??"",
                    onChangeProfileClick: onChangeProfileNameClick);
              },
            ),
          ),
          SizedBox(height: form_field_sepereator_space),
          Container(
            width: MediaQuery.of(context).size.width * .70,
            height: 45,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(8)),
                boxShadow: [
                  BoxShadow(
                      color: const Color(0x29000000),
                      offset: Offset(0, 3),
                      blurRadius: 6,
                      spreadRadius: 0)
                ],
                color: const Color(0xFFE7F5E8)),
            child: TextFormField(
              controller: _phoneNumberController,
              cursorColor: Colors.black,
              keyboardType: TextInputType.phone,
              readOnly: true,
              style: TextStyle(color: french_blue),
              decoration: new InputDecoration(
                hintText: S.of(context).phoneNumber,
              ),

              onTap: (){
                addNewPhoneDialogView(
                    context: context,
                    title: S.of(context).editPhone,
                    currentPhoneNumber:
                    _userProfileResponse?.phone ?? "",
                    secondPhoneNumber: _userProfileResponse?.secondPhone,
                    onChangePhoneClick: changeUserPhone);
              },
            ),
          ),
          //
        ],
      ),
    );
  }



  updateProfileData() {
    if (_profileImagePath != null) {
      updateUserImage();
    }
  }

  getUserData() {
    _userProfileModel.getUserProfileData(
        context: context,
        onSuccess: (response) {
          setState(() {
            _userProfileResponse = response;
            _userNameController.text = _userProfileResponse.username;
            _phoneNumberController.text = _userProfileResponse.phone;
          });
        },
        onError: (error) {
          showError(error);
        });
  }

  changeUserPhone(String phoneNumber,String secondPhoneNumber) {
    if (validatePhoneNumber(phoneNumber)) {
      showLoading(context);
      print("changeUserPhone  $phoneNumber");
      _userProfileModel.changeUserPhone(
          phone_1: phoneNumber,
          phone_2: secondPhoneNumber,
          onSuccess: () {
            showSuccesses(context, S.current.updated);
            hideLoading();
            getUserData();
            Navigator.of(context).pop();
          },
          onError: (error) {
            showError(error.toString());
            hideLoading();
          });
    }
  }

  onChangeProfileNameClick(String firstName, String lastName) {
    showLoading(context);
    var f_name, l_name;
    if (firstName.isEmpty) {
      f_name = Provider.of<AppModel>(context, listen: false).getUserFirstName();
      print("default first name :$f_name");
    } else {
      f_name = firstName;
    }
    if (lastName.isEmpty) {
      l_name = Provider.of<AppModel>(context, listen: false).getUserLastName();
      print("default last name :$l_name");
    } else {
      l_name = lastName;
    }
    print("first name :$f_name --- lastName : $l_name");
    _userProfileModel.editUserFirstNameAndLastName(
        firstName: f_name,
        lastName: l_name,
        onSuccess: () {
          showSuccesses(context, S.current.updated);
          hideLoading();
          getUserData();
          Navigator.of(context).pop();
        },
        onError: (error) {
          showError(error.toString());
          hideLoading();
        });
  }

  updateUserImage() {
    showLoading(context);
    _userProfileModel.updateUserProfileImage(
        profileImagePath: _profileImagePath,
        onSuccess: () {
          hideLoading();
          getUserData();
        },
        onError: (error) {
          hideLoading();
          showError(error);
        });
  }

  validatePhoneNumber(String phoneNumber) {
    if (phoneNumber.length != 11) {
      showError(S.current.invalidEmptyPhoneNumber);
      return false;
    } else {
      return true;
    }
  }
}
