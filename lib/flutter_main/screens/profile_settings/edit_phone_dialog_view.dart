import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_base_app/flutter_main/common/colors.dart';
import 'package:flutter_base_app/flutter_main/common/stats_widgets.dart';
import 'package:flutter_base_app/generated/l10n.dart';

TextEditingController _addNewPhoneTextController = TextEditingController();
TextEditingController _addNewSecondPhoneTextController =
    TextEditingController();

void addNewPhoneDialogView(
    {@required BuildContext context,
    Color barrierColor,
    bool barrierDismissible = false,
    Duration transitionDuration = const Duration(milliseconds: 300),
    Color pillColor,
    String message,
    String title,
    String currentPhoneNumber,
    String secondPhoneNumber,
    Color backgroundColor,
    Function onChangePhoneClick}) {
  showDialog(
      barrierLabel: "Barrier",
      barrierDismissible: true,
      barrierColor: Colors.black.withOpacity(0.5),
      context: context,
      builder: (context) {

        return Dialog(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(25.0)),
          //this right here
          child: Container(
            width: MediaQuery.of(context).size.width * .8,
            height: MediaQuery.of(context).size.height * .40,
            alignment: Alignment.center,
            child: Container(
              alignment: Alignment.center,
              child: renderProfilePhoneForm(context, onChangePhoneClick, title,
                  currentPhoneNumber, secondPhoneNumber),
            ),
          ),
        );
      });
}

renderProfilePhoneForm(BuildContext context, Function onChangePhoneClick, title,
    currentPhoneNumber, secondPhoneNumber) {


  return Container(
    alignment: Alignment.center,
    child: Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text(
          title,
          style: TextStyle(
              color: french_blue,
              fontWeight: FontWeight.w700,
              fontFamily: "Raleway",
              fontStyle: FontStyle.normal,
              fontSize: 18.0),
          textAlign: TextAlign.center,
        ),
        // Rectangle 85
        SizedBox(height: 12.0),

        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              width: MediaQuery.of(context).size.width * .75,
              height: 45,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(24)),
                  color: Color(0xffe7f5e8)),
              child: TextFormField(
                controller: _addNewPhoneTextController,
                keyboardType: TextInputType.phone,
                style: TextStyle(color: french_blue),
                decoration: new InputDecoration(
                    errorStyle: TextStyle(height: 0),
                    border: InputBorder.none,
                    focusedBorder: InputBorder.none,
                    enabledBorder: InputBorder.none,
                    errorBorder: InputBorder.none,
                    disabledBorder: InputBorder.none,
                    contentPadding: EdgeInsets.only(
                        left: 15, bottom: 11, top: 11, right: 15),
                    hintText: S.current.phoneNumber,
                    hintStyle: TextStyle(fontSize: 14)),
              ),
            ),
          ],
        ),
        SizedBox(height: 12.0),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              width: MediaQuery.of(context).size.width * .75,
              height: 45,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(24)),
                  color: Color(0xffe7f5e8)),
              child: TextFormField(
                controller: _addNewSecondPhoneTextController,
                keyboardType: TextInputType.phone,
                style: TextStyle(color: french_blue),
                decoration: new InputDecoration(
                    errorStyle: TextStyle(height: 0),
                    border: InputBorder.none,
                    focusedBorder: InputBorder.none,
                    enabledBorder: InputBorder.none,
                    errorBorder: InputBorder.none,
                    disabledBorder: InputBorder.none,
                    contentPadding: EdgeInsets.only(
                        left: 15, bottom: 11, top: 11, right: 15),
                    hintText: S.current.secondPhone,
                    hintStyle: TextStyle(fontSize: 14)),
              ),
            ),
          ],
        ),

        SizedBox(height: 18.0),
        // Rectangle 85
        ////////////////////
        InkWell(
          child: Container(
            width: MediaQuery.of(context).size.width * .75,
            height: 45,
            alignment: Alignment.center,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(24)),
                boxShadow: [
                  BoxShadow(
                      color: const Color(0x29000000),
                      offset: Offset(0, 3),
                      blurRadius: 6,
                      spreadRadius: 0)
                ],
                color: boring_green),
            child: Text(
              S.of(context).save,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 18.0,
                  fontWeight: FontWeight.w600),
            ),
          ),
          onTap: () {
            if (_addNewPhoneTextController.text.length == 11 &&
                _addNewSecondPhoneTextController.text.length == 11) {
              onChangePhoneClick(_addNewPhoneTextController.value.text,
                  _addNewSecondPhoneTextController.value.text);
            } else if ((_addNewPhoneTextController.text.length != 11)) {
              showError(S.current.invalidPhoneNumber);
            } else if (_addNewPhoneTextController.text.length == 11 &&
                _addNewSecondPhoneTextController.text.length == 0) {
              onChangePhoneClick(_addNewPhoneTextController.value.text, "");
            } else if ((_addNewSecondPhoneTextController.text.length > 0 &&
                _addNewSecondPhoneTextController.text.length != 11)) {
              showError(S.current.invalidSecondPhoneNumber);
            } else {
              print("_addNewPhoneTextController.text --->" +
                  _addNewPhoneTextController.text.toString());
              showError(S.current.invalidPhoneNumber);
            }
          },
        ),
      ],
    ),
  );
}
