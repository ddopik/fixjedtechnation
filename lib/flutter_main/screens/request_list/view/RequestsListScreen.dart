import 'package:flutter/material.dart';
import 'package:flutter_base_app/flutter_main/common/widgets/app_bar_back_button.dart';

import 'RequestsListView.dart';

class RequestsListScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return RequestsListScreenState();
  }
}

class RequestsListScreenState extends State<RequestsListScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        leading: AppBarBackButton(),
        leadingWidth: 200,

      ),
      body: Container(
        child: RequestsListView(),
      ),
    );
  }
}
