import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_base_app/flutter_main/common/colors.dart';
import 'package:flutter_base_app/flutter_main/common/res/dimen_const.dart';
import 'package:flutter_base_app/flutter_main/common/widgets/CircleImageWidget.dart';
import 'package:flutter_base_app/flutter_main/common/widgets/DashedLineVerticalPainter.dart';
import 'package:flutter_base_app/flutter_main/screens/request_list/model/request.dart';
import 'package:flutter_base_app/generated/l10n.dart';
import 'package:intl/intl.dart';

class PastOrderItemView extends StatefulWidget {
  final Transaction request;

  PastOrderItemView({this.request});

  @override
  _PastOrderItemViewState createState() => _PastOrderItemViewState();
}

class _PastOrderItemViewState extends State<PastOrderItemView> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height * .32,
      alignment: Alignment.center,
      padding: EdgeInsets.all(10),
      child: Container(
        alignment: Alignment.center,
        width: MediaQuery.of(context).size.width * .85,
        padding: EdgeInsets.symmetric(horizontal: 10),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(7),
            color: Colors.white.withOpacity(.05000000074505806)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              // color: Colors.yellow,
              width: MediaQuery.of(context).size.width * .18,
              alignment: Alignment.center,
              child: Column(children: [
                SizedBox(
                  height: form_field_space_min,
                ),
                getProfileImage(),
                SizedBox(
                  height: MediaQuery.of(context).size.height * .04,
                  child: CustomPaint(
                      size: Size(1, double.infinity),
                      painter: DashedLineVerticalPainter()),
                ),
                CircleImageWidget(
                  url: widget.request?.categoryUrl ?? "",
                  fit: BoxFit.contain,
                  width: MediaQuery.of(context).size.width * .1,
                  height: MediaQuery.of(context).size.width * .1,
                  borderColor: Colors.white,
                  borderWidth: 2,
                )
              ]),
            ),
            Container(
              padding: EdgeInsets.symmetric(vertical: 10),
              child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width * .60,
                      height: MediaQuery.of(context).size.height * .0225,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          AutoSizeText(
                            widget.request?.customerName ?? "No NAME",
                          ),
                          Row(
                            children: [
                              Text(
                                widget.request?.technicianStatus == "DELIVERED"
                                    ? "completed"
                                    : "cancelled",
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyText1
                                    .copyWith(
                                        color: Colors.white,
                                        fontWeight: FontWeight.w100),
                              ),
                              SizedBox(
                                width: 4,
                              ),
                              Icon(
                                Icons.circle,
                                size: 15,
                                color: widget.request?.technicianStatus ==
                                        "DELIVERED"
                                    ? Colors.green
                                    : Colors.red,
                              )
                            ],
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: form_field_space_min,
                    ),
                    Container(
                        alignment: Alignment.topLeft,
                        width: MediaQuery.of(context).size.width * .55,
                        height: MediaQuery.of(context).size.height * .06,
                        child: AutoSizeText(
                          widget.request?.transactionAddress ?? "",
                          style: Theme.of(context).textTheme.bodyText1.copyWith(
                              color: Colors.white, fontWeight: FontWeight.w100),
                          maxLines: 2,
                        )),
                    Container(
                      width: MediaQuery.of(context).size.width * .27,
                      child: Row(
                        // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          AutoSizeText(S.current.date + " ",
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyText1
                                  .copyWith(
                                      color: Colors.white,
                                      fontWeight: FontWeight.w100)),
                          AutoSizeText(
                              parseTime(widget.request?.assigneeDate ?? ""),
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyText1
                                  .copyWith(
                                    color: Colors.white,
                                  )),
                        ],
                      ),
                    ),
                    widget.request?.technicianStatus == "DELIVERED"
                        ? Container(
                            // width: MediaQuery.of(context).size.width * .2,
                            child: Row(
                              //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                AutoSizeText(S.current.from,
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodyText1
                                        .copyWith(
                                            color: Colors.white,
                                            fontWeight: FontWeight.w100)),
                                SizedBox(
                                  width: 6,
                                ),
                                AutoSizeText(parseTimeMin(
                                    widget.request?.startDate ?? "")),
                                SizedBox(
                                  width: 6,
                                ),
                                AutoSizeText(S.current.to,
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodyText1
                                        .copyWith(
                                            color: Colors.white,
                                            fontWeight: FontWeight.w100)),
                                SizedBox(
                                  width: 6,
                                ),
                                AutoSizeText(parseTimeMin(
                                    widget.request?.endDate ?? "")),
                              ],
                            ),
                          )
                        : Container(
                            // width: MediaQuery.of(context).size.width * .2,
                            child: Row(
                              //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                AutoSizeText(S.current.time,
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodyText1
                                        .copyWith(
                                            color: Colors.white,
                                            fontWeight: FontWeight.w100)),
                                SizedBox(
                                  width: 6,
                                ),
                                widget.request.startDate == ""
                                    ? AutoSizeText(
                                        parseTimeMin(widget.request?.startDate))
                                    : Container(),
                              ],
                            ),
                          ),
                    AutoSizeText(
                      widget.request?.categoryJobType ?? "service name",
                    ),
                    widget.request?.technicianStatus == "DELIVERED"
                        ? Container(
                            width: MediaQuery.of(context).size.width * .6,
                            margin: EdgeInsets.symmetric(vertical: 4),
                            child: Row(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  children: [
                                    Icon(
                                      Icons.star_rate_sharp,
                                      size: 16,
                                      color: 1 <=
                                              (widget.request?.ratingNumber ??
                                                  0)
                                          ? Colors.yellow
                                          : Colors.white,
                                    ),
                                    Icon(Icons.star_rate_sharp,
                                        size: 16,
                                        color: 2 <=
                                                (widget.request?.ratingNumber ??
                                                    0)
                                            ? Colors.yellow
                                            : Colors.white),
                                    Icon(Icons.star_rate_sharp,
                                        size: 16,
                                        color: 3 <=
                                                (widget.request?.ratingNumber ??
                                                    0)
                                            ? Colors.yellow
                                            : Colors.white),
                                    Icon(Icons.star_rate_sharp,
                                        size: 16,
                                        color: 4 <=
                                                (widget.request?.ratingNumber ??
                                                    0)
                                            ? Colors.yellow
                                            : Colors.white),
                                    Icon(Icons.star_rate_sharp,
                                        size: 16,
                                        color: 5 <=
                                                (widget.request?.ratingNumber ??
                                                    0)
                                            ? Colors.yellow
                                            : Colors.white),
                                  ],
                                ),
                                AutoSizeText(
                                  "${widget.request?.totalCost ?? 0}\$",
                                )
                              ],
                            ))
                        : Container(
                            width: MediaQuery.of(context).size.width * .6,
                            height: 30,
                            child: AutoSizeText("سبب الرفض"),
                          ),
                    InkWell(
                      onTap: () {},
                      child: Container(
                        height: 12,

                        child: AutoSizeText(
                          "REPORT A PROBLEM",
                          style: Theme.of(context)
                              .textTheme
                              .subtitle2
                              .copyWith(color: boring_green),
                        ),
                      ),
                    ),
                  ]),
            )
          ],
        ),
      ),
    );
  }

  getProfileImage() {
    return CircleImageWidget(
      url: widget.request?.customerImageUrl ?? default_profile_img,
      fit: BoxFit.contain,
      width: MediaQuery.of(context).size.width * .16,
      height: MediaQuery.of(context).size.width * .16,
      borderColor: Colors.white,
      borderWidth: 2,
    );
  }

  String parseTime(String timeVal) {
    String datetime = timeVal;
    // String offset = data["utc_offset"].substring(1, 3);
    DateTime now = DateTime.parse(datetime);
    // now = now.add(Duration(hours: int.parse(offset)));
    return DateFormat("yyyy-MM-dd").format(now);
  }

  String parseTimeMin(String timeVal) {
    String datetime = timeVal;
    // String offset = data["utc_offset"].substring(1, 3);
    DateTime now = DateTime.parse(datetime);
    // now = now.add(Duration(hours: int.parse(offset)));
    return DateFormat("hh:mm").format(now);
  }
}
