import 'package:flutter/cupertino.dart';
import 'package:flutter_base_app/flutter_main/app/app_model.dart';
import 'package:flutter_base_app/flutter_main/screens/profile_settings/model/user_profile_response.dart';
import 'package:flutter_base_app/network/dio_manager.dart';
import 'package:provider/provider.dart';

class UserProfileModel extends ChangeNotifier{


  getUserProfileData({BuildContext context, onSuccess, onError}) {
    DIOManager().getProfileData(onSuccess: (response) {
      UserProfileResponse userProfileResponse =
      UserProfileResponse.fromJson(response);
      saveUserDate(context, userProfileResponse);
      onSuccess(userProfileResponse);
    }, onError: (errorResponse) {
      onError(errorResponse.toString());
    });
  }


  editUserFirstNameAndLastName(
      {context, onSuccess, onError, firstName, lastName}) {
    DIOManager().editUserFirstNameAndLastName(
        firstName: firstName,
        lastName: lastName,
        onSuccess: (response) {
          onSuccess();
        },
        onError: (errorResponse) {
          onError(errorResponse.toString());
        });
  }


  changeUserPhone({context, onSuccess, onError, phone_1, phone_2}) {
    DIOManager().changePhone(
        phone: phone_1,
        secondPhone: phone_2,
        onSuccess: (response) {
          onSuccess();
        },
        onError: (errorResponse) {
          onError(errorResponse.toString());
        });
  }
  updateUserProfileImage({profileImagePath, onSuccess, onError}) {
    DIOManager().changeUserProfileImage(
        imagePath: profileImagePath,
        onSuccess: (response) {
          onSuccess();
        },
        onError: (errorResponse) {
          onError(errorResponse.toString());
        });
  }

  saveUserDate(context, response) {


    Provider.of<AppModel>(context, listen: false)
        .setUserId(response.id.toString);

    Provider.of<AppModel>(context, listen: false)
        .setUserMail(response.email.toString());

    Provider.of<AppModel>(context, listen: false)
        .setUserPhone(response.phone.toString());



    Provider.of<AppModel>(context, listen: false)
        .setUserName(response.username.toString());

    Provider.of<AppModel>(context, listen: false)
        .setUserFirstName(response.firstName.toString());

    Provider.of<AppModel>(context, listen: false)
        .setUserLastName(response.lastName.toString());

    Provider.of<AppModel>(context, listen: false)
        .setUserProfileImage(response.imageUrl.toString());

    Provider.of<AppModel>(context, listen: false).setIsUserLoggedIn(true);

    print("saveUserDate ----> Passed");
  }
}
