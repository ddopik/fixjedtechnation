import 'package:flutter_base_app/flutter_main/screens/cancel_request/reason/cancel_request_reason.dart';
import 'package:flutter_base_app/generated/l10n.dart';
import 'package:flutter_base_app/network/dio_manager.dart';

class CancelRequestProvider {
  List<CancelRequestReason> getReasonList() {
    return [
      CancelRequestReason(
          id: "CUSTOMER_NOT_AT_HOME",
          reason: S.current.customerWasntAthome),
      CancelRequestReason(
          id: "CUSTOMER_NOT_ANSWER_CALLS",
          reason: S.current.customer_did_not_tAnswer),
      CancelRequestReason(
          id: "CUSTOMER_ASKED_ME_TO_CANCEL",
          reason: S.current.customerAskedToCancel),
      CancelRequestReason(
          id: "CUSTOMER_IS_RUDE", reason: S.current.customerIsRude),
      CancelRequestReason(
          id: "OTHER_OPERATIONS_TO_CONTACT_ME",
          reason: S.current.othersAndIneedOneofOperationstoContactMe),
    ];
  }

  submitCancelRunningTransaction(
      {Function onSuccess, Function onError, transactionId, reasonId}) {
    DIOManager().submitCancelRunningTransactionRequest(
        onSuccess: (response) {
          onSuccess(response);
        },
        onError: (response) {
          onError(response);
        },
        reasonId: reasonId.toString(),
        transactionId: transactionId);
  }
}

enum DeliveredCancelReason {
  CUSTOMER_NOT_AT_HOME,
  CUSTOMER_NOT_ANSWER_CALLS,
  CUSTOMER_ASKED_ME_TO_CANCEL,
  CUSTOMER_IS_RUDE,
  OTHER_OPERATIONS_TO_CONTACT_ME
}
