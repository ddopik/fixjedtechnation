class CancelRequestReason {
  String _id;
  String _reason;

  CancelRequestReason({
    String id,
    String reason}) {
    _id = id;
    _reason = reason;
  }


  String get id => _id;

  String get reason => _reason;


  CancelRequestReason.fromJson(dynamic json) {
    _id = json["id"];
    _reason = json["reason"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["reason"] = _reason;
    return map;
  }

}