import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_base_app/flutter_main/app/route.dart';
import 'package:flutter_base_app/flutter_main/common/colors.dart';
import 'package:flutter_base_app/flutter_main/common/res/dimen_const.dart';
import 'package:flutter_base_app/flutter_main/common/res/font_const.dart';
import 'package:flutter_base_app/flutter_main/common/stats_widgets.dart';
import 'package:flutter_base_app/flutter_main/common/widgets/app_bar_back_button.dart';
import 'package:flutter_base_app/flutter_main/screens/cancel_request/provider/cancel_request_provider.dart';
import 'package:flutter_base_app/flutter_main/screens/cancel_request/reason/cancel_request_reason.dart';
import 'package:flutter_base_app/flutter_main/screens/request_list/provider/TransactionModel.dart';
import 'package:flutter_base_app/generated/l10n.dart';
import 'package:provider/provider.dart';

class CancelRequestScreen extends StatefulWidget {
  final transaction;

  CancelRequestScreen({this.transaction});

  @override
  State<StatefulWidget> createState() {
    return CancelRequestScreenState();
  }
}

class CancelRequestScreenState extends State<CancelRequestScreen> {
  var reasonId;
  List<CancelRequestReason> reasonsList = [];
  CancelRequestProvider cancelRequestProvider = CancelRequestProvider();

  @override
  void initState() {
    reasonsList = cancelRequestProvider.getReasonList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // leadingWidth: 200,
        leading: AppBarBackButton(),
        elevation: 0.0,
      ),
      body: Container(
        alignment: Alignment.topCenter,
        child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                        width: MediaQuery
                            .of(context)
                            .size
                            .width * .62,
                        padding:
                        EdgeInsets.all(inner_boundary_field_space_wide),
                        child: AutoSizeText(S.current.cancellationReason,
                            style:
                            Theme
                                .of(context)
                                .textTheme
                                .headline4
                                .copyWith(
                              color: Color(0xffffffff),
                            ))),
                    Container(
                        width: MediaQuery
                            .of(context)
                            .size
                            .width,
                        height: MediaQuery
                            .of(context)
                            .size
                            .height * .5,
                        padding: EdgeInsets.symmetric(horizontal: 16),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(7),
                        ),
                        child: ListView.builder(
                            itemCount: reasonsList.length,
                            itemBuilder: (context, i) {
                              return Row(
                                children: [
                                  Radio(
                                      activeColor: nasty_green,
                                      fillColor: MaterialStateProperty.all(
                                          nasty_green),
                                      value: reasonsList[i].id,
                                      groupValue: reasonId,
                                      onChanged: (val) {
                                        setState(() {
                                          reasonId = reasonsList[i].id;
                                        });
                                      }),
                                  Expanded(
                                      child: Text('${reasonsList[i].reason}',
                                          textAlign: TextAlign.start,
                                          style: Theme
                                              .of(context)
                                              .textTheme
                                              .subtitle2
                                              .copyWith(
                                              color: Color(0xffffffff)))),
                                ],
                              );
                            })),
                  ],
                ),
              ),
              Container(),
              InkWell(
                child: Container(
                    height: MediaQuery
                        .of(context)
                        .size
                        .height * .05,
                    width: MediaQuery
                        .of(context)
                        .size
                        .width * .86,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                        color: nasty_green,
                        borderRadius: BorderRadius.circular(8)),
                    child: AutoSizeText(
                      S.current.submit,
                      style: Theme
                          .of(context)
                          .textTheme
                          .subtitle2
                          .copyWith(color: Colors.white, fontSize: text_size_1),
                    )),
                onTap: () {
                  if (reasonId == null) {
                    showError(S.current.pleaseSelectReason);
                  } else {
                    showLoading(context);
                    cancelRequestProvider.submitCancelRunningTransaction(
                        reasonId: reasonId,
                        transactionId: widget.transaction
                            .technicianTransactionId,
                        onSuccess: (response) {
                          hideLoading();
                          Provider
                              .of<TransactionModel>(context, listen: false)
                              .allConfirmedRequest
                              .clear();
                          Navigator.of(context)
                              .pushNamed(Routes.ORDER_SUBMISSION_FEEDBACK);
                        },
                        onError: (response) {
                          hideLoading();
                          showError(response);
                        });
                  }
                },
              ),
              Container()
            ]),
      ),
    );
  }
}
